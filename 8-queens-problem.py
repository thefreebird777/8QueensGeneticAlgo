import numpy as np
import random


def fitness(Input):
    # Input should be a list

    # Step1: make the state out of the Input
    state = np.zeros((8, 8))
    for j in range(8):
        state[Input[j] - 1][j] = 1

    # Step2: find the fitness of the state
    attacks = 0
    k = -1
    for j in range(8):
        k += 1
        # direction 1: the east
        for l in range(k + 1, 8):
            attacks += state[state[:, j].argmax()][l]

        # direction 2: the northeast
        row = state[:, j].argmax()
        column = j
        while row > 0 and column < 7:
            row -= 1
            column += 1
            attacks += state[row][column]

        # direction 3: the southeast
        row = state[:, j].argmax()
        column = j
        while row < 7 and column < 7:
            row += 1
            column += 1
            attacks += state[row][column]

    return 28 - attacks
#creates a population of random boards
def populate():
    boards = []
    i=0
    while i < 150:
        j=0
        board = []
        while j < 8:
            board.append(random.randint(1,8))
            j += 1
        boards.append(board)
        i += 1
    return boards
#probabilty is found by finding the fitness sum then dividing each fitness by the sum
def probability(fit):
    sum = 0
    probabilities = []
    for f in fit:
        sum += f
    for f in fit:
        probabilities.append(f/sum)
    return probabilities
#chooses 4 parents based on probabilty
def chooseParents(probs):
    parent1 = np.random.choice(150,1,False,probs)
    parent2 = np.random.choice(150,1,False,probs)
    parent3 = np.random.choice(150,1,False,probs)
    parent4 = np.random.choice(150,1,False,probs)
    parents = (parent1[0], parent2[0], parent3[0], parent4[0])
    return parents

def nextGen(p1, p2, p3, p4):
    boards = []
    i = 0
    while i < 150:
        #decides where parents cross
        cross = random.randint(0, 7)

        c1 = p1[:cross] + p2[cross:8]
        c2 = p2[:cross] + p1[cross:8]
        c3 = p1[:cross] + p3[cross:8]
        c4 = p3[:cross] + p1[cross:8]
        c5 = p1[:cross] + p4[cross:8]
        c6 = p4[:cross] + p1[cross:8]
        c7 = p2[:cross] + p3[cross:8]
        c8 = p3[:cross] + p2[cross:8]
        c9 = p2[:cross] + p4[cross:8]
        c10 = p4[:cross] + p2[cross:8]
        c11 = p3[:cross] + p4[cross:8]
        c12 = p4[:cross] + p3[cross:8]

        children = [c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12]
        for child in children:
            board = list(child)
            mutation = np.random.choice(2, 1, False, [0.2,0.8]) #80% mutation rate
            #0 is no mutation 1 is mutation
            if mutation[0] == 1:
                #mutates by changing one column at random to a random row
                j = random.randint(0, 7)
                board[j] = random.randint(1, 8)
                boards.append(board)
                i += 1
                if i == 150:
                    break
    return boards

if __name__ == '__main__':
    boards = []
    boards = populate()
    solved = False
    solution = []
    gens = 0
    while not solved:
        #if generation gets too high the program starts over. this has never happened
        if gens == 2500:
            boards = populate()
            gens = 0
        probabilities = []
        fit = []
        for board in boards:
            f = fitness(board)
            if f == 28: #solution is found
                solution = board
                solved = True
            fit.append(f)
        if solved is True:
            break
        probabilities = list((probability(fit)))
        parents = chooseParents(probabilities)
        p1 = boards[parents[0]]
        p2 = boards[parents[1]]
        p3 = boards[parents[2]]
        p4 = boards[parents[3]]
        boards = []
        boards = nextGen(p1, p2, p3, p4)
        gens += 1
    print solution